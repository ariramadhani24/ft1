package FT1;

import java.util.Scanner;

public class soal10_DeretAngka {

	public static void main(String[] args) {
//		kelipatan 3 dikurang 1 
//		kelipatan 4 dibagi 2
//		index ganjil dijumlahkan
//		index genap dijumlahkan
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Masukan panjang deret : ");
		int in = sc.nextInt();

		int kel3k2[] = new int[in]; // ganjil
		int kel4b2[] = new int[in]; // genap

		int genap = 0, ganjil = 0;

		
		for (int i = 0; i < in; i++) {
			kel3k2[i] = i*3-1;
			kel4b2[i] = i*4/2;
		}

//		for (int i = 0; i < kel4b2.length; i++) {
//			System.out.print(kel3k2[i] + " ");
//			System.out.println(kel4b2[i]);
//		}

//		for (int i = 0; i < kel4b2.length; i++) {
//			if ((i+1) % 2 == 0) {
//				genap += kel4b2[i] + kel3k2[i];
//			} else {
//				ganjil += kel4b2[i] + kel3k2[i];
//			}
//		}

		System.out.print("Bilangan dikali 3 kurang 1 : ");
		for (int i = 0; i < kel4b2.length; i++) {
			System.out.print(kel4b2[i] + " ");
		}
		
		System.out.println();

		System.out.print("Bilangan dikali 4 dibagi 2 : " );
		for (int i = 0; i < kel3k2.length; i++) {
			System.out.print(kel3k2[i] + " ");
		}
		
//		System.out.println();
//		System.out.print("Total ganjil : "+ ganjil);
//		System.out.println();
//		System.out.print("Total genap : "+ genap);

		System.out.println();
		
		for (int i = 0; i < kel4b2.length; i++) {
			System.out.print(kel4b2[i]+" + "+kel3k2[i]+", ");
		}
		
		System.out.println();
		
		for (int i = 0; i < kel4b2.length; i++) {
			System.out.print(kel4b2[i]+kel3k2[i]+", ");
		}
		
		sc.close();
		
	}

}
