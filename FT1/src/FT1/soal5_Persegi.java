package FT1;

import java.util.Scanner;

public class soal5_Persegi {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int jmlSisi= 6;
		
		System.out.print("Masukan panjang sisi (cm): ");
		int n = sc.nextInt(); //persegi
		System.out.print("Masukan panjang rusuk (cm): ");
		int m = sc.nextInt(); //rusuk
		
		int totalPersegi = (m/n)*(m/n)*jmlSisi;
		
		System.out.println("Jumlah bangun persegi / jumlah sisi kubus = " + totalPersegi);
		
		sc.close();
	}

}
