package FT1;

import java.util.Scanner;

public class soal7_bobotAlfabet {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Masukan kata / kalimat :");
		String sInput = sc.nextLine().toLowerCase();

		char [] aSInput = sInput.toCharArray();

		int [] aIntInput = new int[aSInput.length];
		
		for (int i = 0; i < aSInput.length; i++) {
			aIntInput[i] = 27 - (Integer.valueOf(aSInput[i]) - 96);
		}


		int jArray [] = new int[aIntInput.length];
		
		for (int i = 0; i < aIntInput.length; i++) {
			System.out.println("Masukan nilai di array index "+i+" : ");
			jArray[i] = sc.nextInt();			
		}
		
		
		
		//Verifikasi
		
		boolean hasil[] = new boolean[jArray.length];
		
		for (int i = 0; i < jArray.length; i++) {
			if (aIntInput[i] == jArray[i]) {
				hasil[i] = true;
			} else {
				hasil[i] = false;
			}
		}

		for (boolean b : hasil) {
			System.out.print(b+" , ");
		}
		
		sc.close();
	}
}
