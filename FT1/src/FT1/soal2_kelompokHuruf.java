package FT1;

import java.util.Scanner;

public class soal2_kelompokHuruf {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Masukan kalimat : ");
		String inKata = sc.nextLine().toLowerCase();
		inKata = inKata.replaceAll(" ","");
		char[] aInKata = inKata.toCharArray();
		
		System.out.println(aInKata);

		bubblesortReverse(aInKata);
		
		System.out.println(aInKata);
		
//		for (int i = 0; i < args.length; i++) {
//			
//		}
		for (char c : aInKata) {
			System.out.print(c);
		}

		System.out.println();
		
		for (int i = 0; i < aInKata.length-1; i++) {
			if (i == aInKata.length-2) {
				System.out.println(aInKata[i+1]);
			} else if (aInKata[i] != aInKata[i+1] && i != aInKata.length - 1) {
				System.out.print(aInKata[i]);
				System.out.print("-");
			} else {
				System.out.print(aInKata[i]);
			}
		}
		
		sc.close();
	}

	public static void bubblesort(char[] val) {
		for (int i = 0; i < val.length - 1; i++) {
			for (int j = 0; j < val.length - 1 - i; j++) {
				Character cont = val[j];
				if (val[j] > val[j + 1]) {
					val[j] = val[j + 1];
					val[j + 1] = cont;
				} 				
			}
		}
	}


	public static void bubblesortReverse(char[] val) {
		for (int i = 0; i < val.length - 1; i++) {
			for (int j = 0; j < val.length - 1 - i; j++) {
				Character cont = val[j];
				if (val[j] < val[j + 1]) {
					val[j] = val[j + 1];
					val[j + 1] = cont;
				} 				
			}
		}
	}
}
