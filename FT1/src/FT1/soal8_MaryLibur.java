package FT1;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class soal8_MaryLibur {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Masukan x hari : ");
		int x = sc.nextInt();
		
		System.out.println("Masukan y hari : ");
		int y = sc.nextInt();

		int bertemuLagi = 0;		
		
		int contY = y, contX = x;
		
		int i=1,j=1;
		while(true) {
			if (contX!=contY) {
				if (contX>contY) {
					contY = i*y;
					i++;
				}else if (contX<contY) {
					contX = j*x;
					j++;
				}
			} else {
				bertemuLagi = contX;
				break;
			}
//			System.out.println(contX+" | "+contY);
		}

		SimpleDateFormat dfd = new SimpleDateFormat("dd");
		Date dBertemulagi = dfd.parse(String.valueOf(bertemuLagi));
		
		sc.nextLine();
		System.out.println("Masukan tanggal terakhir libur bersama : ");
		String z = sc.nextLine();
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
				
		Date dTglA = df.parse(z);

		long hariBertemuLagi = dTglA.getTime()+dBertemulagi.getTime();
		
		System.out.println(df.format(hariBertemuLagi));
				
		System.out.println();
				
		sc.close();
	}

}
