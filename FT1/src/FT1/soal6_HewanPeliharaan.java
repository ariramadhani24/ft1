package FT1;

public class soal6_HewanPeliharaan {

	public static void main(String[] args) {
		String naikturun = "N T T N N N NN T T N N T T  N N";
		naikturun = naikturun.replaceAll(" ", "");

		char[] aNaikturun = naikturun.toCharArray();
		
		int ketinggian = 0;
		
		int gunung = 0;
		int lembah = 0;
		
		int tertinggi = 0;
		int terendah = 0;
		
		for (int i = 0; i < aNaikturun.length; i++) {
			System.out.print(ketinggian+" ");
			if (aNaikturun[i] == 'N') {
				ketinggian ++;
				if (ketinggian == 0) {
					lembah++;
				}
				if (ketinggian > tertinggi) {
					tertinggi = ketinggian;
				}
			} else if (aNaikturun[i] == 'T') {
				ketinggian --;
				if (ketinggian == 0) {
					gunung++;
				}
				if (ketinggian < terendah) {
					terendah = ketinggian;
				}
			}			
		}

		System.out.println("Berapa Gunung dan Lembah yang sudah dilewati Chester ? "+gunung+" gunung dan "+lembah+" lembah");
		System.out.println("Tertinggi : "+tertinggi+", Terendah "+terendah);
	}

}
