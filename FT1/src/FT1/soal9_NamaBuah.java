package FT1;

import java.util.Scanner;

public class soal9_NamaBuah {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nama buah : ");
		String nmBuah = sc.nextLine().toLowerCase();

		String[] aNmBuah = nmBuah.split("");

		
		int jml = 0;

		for (String string : aNmBuah) {
			System.out.print(string+" ");
			if (string.equals("a") || string.equals("i") ||string.equals("u") ||string.equals("e") ||string.equals("o")) {
				jml++;
				System.out.println(jml);
			}
		}
		
		int hargaBuah = 10000 * jml;
		
		System.out.println("Harga buah : " + hargaBuah + " per Kg");		
		
		sc.close();
	}

}
